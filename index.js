'use strict';
//1. Функция цельных механизм который можно вызывать в разных ситуациях или с разными аргументами, которая выполняет определенное действие помогающее сделать нам задуманное
//2. Не каждая функция принимает аргументы, но если принимает, то для того чтобы использовать их внутри при этом переиспользовав с любыми другими значениями
//3. Он позволяет выйти из функции вернув переменную/значение/андейафнд 
const calcMath = () => {
  let frstNum = Number(prompt('Write first number'));
  let scdNum = Number(prompt('Write second number'));
  while ((isNaN(frstNum) || isNaN(scdNum)) === true || (frstNum || scdNum) === '') {
    alert(`Write correct number! *${frstNum}* or *${scdNum}* is wrong!`)
    frstNum = Number(prompt('Write first number'));
    scdNum = Number(prompt('Write second number'));
  }
  let takeSymbol = prompt('Write math symbol');
  console.log(takeSymbol);
  console.log(typeof takeSymbol);
  while (takeSymbol !== '+' && takeSymbol !== '-' && takeSymbol !== '*' && takeSymbol !== '/') {
    takeSymbol = prompt('Write math symbol');
    console.log(takeSymbol);
    console.log(typeof takeSymbol);
  }
  switch (true) {
    case takeSymbol === '+':
      return frstNum + scdNum
    case takeSymbol === '-':
      return frstNum - scdNum
    case takeSymbol === '*':
      return frstNum * scdNum
    case takeSymbol === '/':
      return frstNum / scdNum
  }

}
console.log(calcMath());